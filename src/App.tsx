import React from 'react';
import logo from './logo.svg';
import Debounce from './utils/debounce'
import './App.css';

class App extends React.Component { 
  
@Debounce(100, true)
callBack(){
  console.log("callBack")
}

render(){
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

}

export default App;
